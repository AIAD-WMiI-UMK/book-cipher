#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyperclip

from simplecrypto.bookcipher import encrypt, decrypt


def main():
	message = input("Enter plaintext or ciphertext: ") 
	process = input("Enter 'encrypt' or 'decrypt': ")
	while process not in ('encrypt', 'decrypt'):
		process = input("Invalid process. Enter 'encrypt' or 'decrypt': ")
	shift = int(input("Shift value (1-366) = "))
	while not 1 <= shift <= 366:
		shift = int(input("Invalid value. Enter digit from 1 to 366: "))

	if process == 'encrypt':
		ciphertext = encrypt(message, shift)

		print(f"encrypted ciphertext = \n {ciphertext}\n")
		try:
			pyperclip.copy(ciphertext)
		except pyperclip.PyperclipException:
			pass

	elif process == 'decrypt':
		plaintext = decrypt(message, shift)

		print(f"ecrypted plaintext = \n {plaintext}\n")
    

if __name__ == '__main__':
    main()
