#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
import os
import random
import sys


def encrypt(message, key):
	"""Return list of indexes representing characters in a message."""
	infile = 'simplecrypto/lost.txt'
	if not os.path.exists(infile):
		print("File {} not found. Terminating.".format(infile), file=sys.stderr)
		sys.exit(1)

	book_text = load_file(infile)
	char_dict = make_dict(book_text, key)

	encrypted = []
	for char in message.lower():
		if len(char_dict[char]) > 1:
			index = random.choice(char_dict[char])
		elif len(char_dict[char]) == 1:  # Random.choice fails if only 1 choice.
			index = char_dict[char][0]
		elif len(char_dict[char]) == 0:
			print("\nCharacter {} not in dictionary.".format(char),
			      file=sys.stderr)
			continue      
		encrypted.append(index)
	return encrypted


def decrypt(message, key):
	"""Decrypt ciphertext list and return plaintext string."""
	infile = 'simplecrypto/lost.txt'
	if not os.path.exists(infile):
		print("File {} not found. Terminating.".format(infile), file=sys.stderr)
		sys.exit(1)

	book_text = load_file(infile)
	
	plaintext = ''
	indexes = [s.replace(',', '').replace('[', '').replace(']', '')
               for s in message.split()]
	for i in indexes:
		plaintext += book_text[int(i) - key]
	return plaintext


def load_file(infile):
    """Read and return text file as a string of lowercase characters."""
    with open(infile) as f:
        loaded_string = f.read().lower()
    return loaded_string


def make_dict(text, shift):
    """Return dictionary of characters as keys and shifted indexes as values."""
    char_dict = defaultdict(list)
    for index, char in enumerate(text):
        char_dict[char].append(index + shift)
    return char_dict
